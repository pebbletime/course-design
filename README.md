# 1.简介

一个手机通讯录项目,主要采用Material Design + recyclerview 实现了联系人本地导入导出，从服务器下载联系人信息，拨打电话等功能。

# 2.部署步骤：
修改下列配置，替换配置中的XXX版本号为你本级的版本。

**1.修改app/build.gradle**

```groovy
compileSdkVersion XXX
buildToolsVersion "XXX"
targetSdkVersion XXX
```

**2.修改/buildgradle**

```groovy
classpath "com.android.tools.build:gradle:XXX"
```

**3.修改/gradle/wrapper/gradle-wrapper.properties**

```groovy
distributionUrl=https\://services.gradle.org/distributions/gradle-XXX-bin.zip
```

即可导入AS