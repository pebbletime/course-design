package com.pan.coursedesign.recyclerview;


import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pan.coursedesign.R;
import com.pan.coursedesign.recyclerview_logs.recording;

import org.litepal.crud.DataSupport;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.pan.coursedesign.utils.MobileNumberUtils.getCarrier;
import static com.pan.coursedesign.utils.MobileNumberUtils.getGeo;

public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.ViewHolder> {

    private List<Person> mPersonList;//数据源
    AlertDialog.Builder builder5;
    AlertDialog dialog;


    public PersonAdapter(List<Person> PersonList) {//1.传递来一个数据源存储起来
        mPersonList = PersonList;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        View PersonView;
        TextView PersonName;
        TextView PersonTel;
        TextView PersonEmail;
        Button Personbtn1;
        Button Personbtn2;
        public ViewHolder(View view) {
            super(view);
            PersonView = view;
            PersonName = (TextView) view.findViewById(R.id.Person_name);
            PersonTel = (TextView) view.findViewById(R.id.Person_tel);
            PersonEmail= (TextView) view.findViewById(R.id.Person_email);
            Personbtn1 = (Button) view.findViewById(R.id.btn_xg);
            Personbtn2 = (Button) view.findViewById(R.id.btn_sc);
        }
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {  //2.获取一个item
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.person_item, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        //点击item拨打电话
        holder.PersonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                Person person = mPersonList.get(position);//通过位置获取数据

                //打电话
                Intent intent = new Intent(Intent.ACTION_CALL);//调用系统内置好的action来启动系统活动（隐式Intent的更多用法）
                intent.setData(Uri.parse("tel:"+person.getTel()));//设置一个Uri对象作为操作数据。这个数据可以被定义额外定义data的活动响应
                v.getContext().startActivity(intent);

                //打完要记录到数据库哦!!!!!!!
                recording recording=new recording();
                recording.setName(person.getName());
                recording.setTel(person.getTel());
                //获取归属地getGeo(String phoneNumber)
                recording.setPosition(getGeo(person.getTel()));
                //获取打电话时间
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
                recording.setTime(df.format(new Date()));//new Date()为获取当前系统时间
                //运营商获取
                recording.setStatus(getCarrier(view.getContext(), person.getTel(),86));
                //保存到数据库
                recording.save();
            }
        });




        //修改按钮
        holder.Personbtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                Person Person = mPersonList.get(position);//通过位置获取list的数据
                //跳转到信息页面进行修改,通过parent.getContext()获取上下文对象
                //inflate方法返回的是一个view（自定义那个），
                View loginview = LayoutInflater.from(parent.getContext()).inflate(R.layout.contacts_dialog_infox, null);
                //loginview里的控件实例化一下
                EditText username = loginview.findViewById(R.id.et_username);
                EditText password = loginview.findViewById(R.id.et_password);
                EditText email = loginview.findViewById(R.id.et_email);
                Button btnLogin = loginview.findViewById(R.id.btn_login);

                //先反向加载信息，将数据写入
                username.setText(Person.getName());
                password.setText(Person.getTel());
                email.setText(Person.getEmail());

                btnLogin.setOnClickListener(new View.OnClickListener() {
                    //再保存修改到数据库，
                    @Override
                    public void onClick(View view) {
                        int position = holder.getAdapterPosition();
                        Person Person = mPersonList.get(position);
                        //数组里删除
                        mPersonList.remove(Person);//修改一定同步到list
                        //数据库中删除,通过名字指定
                        String name = Person.getName();
                        DataSupport.deleteAll(Person.class,"name = ?", name);
                        //删除后保存新数据
                        Person Person2 = new Person();
                        Person2.setName(username.getText().toString());
                        Person2.setTel(password.getText().toString());
                        Person2.setEmail(email.getText().toString());
                        Person2.save();
                        mPersonList.add(Person2);//修改一定同步到文件

                        Toast.makeText(v.getContext(), "修改" + Person.getName() + "成功", Toast.LENGTH_LONG).show();
                        //notifyItemInserted(position);
                        notifyDataSetChanged();//刷新一下(数组变了)
                        dialog.dismiss();//关闭自定义对话框
                    }
                });
                //设置Dialog和View
                builder5 = new AlertDialog.Builder(parent.getContext());
                dialog = builder5.setTitle("修改联系人信息").setView(loginview).show();
            }
        });

        //删除按钮
        holder.Personbtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                Person Person = mPersonList.get(position);
                //数组里删除
                mPersonList.remove(Person);
                //数据库中删除,通过名字指定
                String name = Person.getName();
                DataSupport.deleteAll(Person.class,"name = ?", name);
                Toast.makeText(v.getContext(), "删除 " + Person.getName() + "成功！", Toast.LENGTH_SHORT).show();
                notifyItemRemoved(position);//刷新
            }
        });

        return holder;//返回item实例
    }



    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {  //3.进行数据绑定
        Person Person = mPersonList.get(position);//取出数据给对象
        //从对象取出数据给item的控件进行数据绑定
        holder.PersonName.setText("姓名：" + Person.getName());
        holder.PersonTel.setText("电话：" + Person.getTel());
        holder.PersonEmail.setText("电子邮件：" + Person.getEmail());
    }


    //告诉RecyclerView一共有多少子项
    @Override
    public int getItemCount() {
        return mPersonList.size();
    }

}