package com.pan.coursedesign.recyclerview;

import org.litepal.crud.DataSupport;

public class Person extends DataSupport {
    private String name;
    private String Tel;
    private String Email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return Tel;
    }

    public void setTel(String tel) {
        Tel = tel;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }


    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", Tel='" + Tel + '\'' +
                ", Email='" + Email + '\'' +
                '}';
    }
}
