package com.pan.coursedesign.recyclerview;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.pan.coursedesign.R;
import com.pan.coursedesign.recyclerview_logs.recording;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.pan.coursedesign.utils.MobileNumberUtils.getCarrier;
import static com.pan.coursedesign.utils.MobileNumberUtils.getGeo;

//我们手动定义一个ViewHolder静态内部类作为范性要求的子类
public class PersonAdapter_info extends RecyclerView.Adapter<PersonAdapter_info.ViewHolder> {

    private List<Person> mPersonList;//数据源


    //1.传递来一个数据源存储起来
    public PersonAdapter_info(List<Person> PersonList) {
        mPersonList = PersonList;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {//内部类，指定泛型为ViewHolder的子类
        View PersonView;
        //缓存组件实例
        TextView PersonName;
        TextView PersonTel;
        TextView PersonEmail;
        //重写构造
        public ViewHolder(View view) {//view通常是RecyclerView子项的最外层布局
            super(view);
            //Log.d("bbbbb", "ViewHolder: ");
            PersonView = view;//把控件都实例化
            PersonName = (TextView) view.findViewById(R.id.Person_name);
            PersonTel = (TextView) view.findViewById(R.id.Person_tel);
            PersonEmail= (TextView) view.findViewById(R.id.Person_email);
        }
    }



    //创建viewholder也就创建出一个item架子，并把viewholder的item返回出去
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {  //2.获取一个item
        //R.layout.Person_item布局文件反射为对象（对布局文件的控件进行管理） 创建架子,只展示信息的架子
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.person_search_item, parent, false);

        //创建一个ViewHolder实例，把布局传入到构造函数中把架子的控件存起来
        final ViewHolder holder = new ViewHolder(view);

        //测试勿删
        //Log.d("aaaa----bbb", "onCreateViewHolder");

        //点击item拨打电话
        holder.PersonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                //Log.d("aaaa----bbb", "onCreateViewHolder" + position);
                Person person = mPersonList.get(position);//通过位置获取数据

                Intent intent = new Intent(Intent.ACTION_CALL);//调用系统内置好的action来启动系统活动（隐式Intent的更多用法）
                intent.setData(Uri.parse("tel:"+person.getTel()));//设置一个Uri对象作为操作数据。这个数据可以被定义额外定义data的活动响应
                v.getContext().startActivity(intent);

                recording recording=new recording();
                recording.setName(person.getName());
                recording.setTel(person.getTel());
                recording.setPosition(getGeo(person.getTel()));
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
                recording.setTime(df.format(new Date()));//new Date()为获取当前系统时间
                recording.setStatus(getCarrier(view.getContext(), person.getTel(),86));
                recording.save();
            }
        });
        return holder;//返回item实例
    }



    //每个子项滚动到屏幕内的时候执行onBindViewHolder为RecyclerView的子项数据进行赋值
    //当viewholder和数据绑定的时候回调（滚动）
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {  //3.进行数据绑定
        //Log.d("aaaa----bbb", "onBindViewHolder" + position);
        Person Person = mPersonList.get(position);//取出数据给对象
        //从对象取出数据给item的控件进行数据绑定
        holder.PersonName.setText("姓名：" + Person.getName());
        holder.PersonTel.setText("电话：" + Person.getTel());
        holder.PersonEmail.setText("电子邮件：" + Person.getEmail());
    }


    //告诉RecyclerView一共有多少子项
    @Override
    public int getItemCount() {
        return mPersonList.size();
    }

}