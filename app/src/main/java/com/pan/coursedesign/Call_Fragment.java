package com.pan.coursedesign;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pan.coursedesign.recyclerview.Person;
import com.pan.coursedesign.recyclerview.PersonAdapter_info;
import com.pan.coursedesign.recyclerview_logs.recording;

import org.litepal.LitePal;
import org.litepal.crud.DataSupport;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.pan.coursedesign.utils.MobileNumberUtils.getCarrier;
import static com.pan.coursedesign.utils.MobileNumberUtils.getGeo;


public class Call_Fragment extends Fragment implements View.OnClickListener{
    View view;

    PersonAdapter_info adapter;//指定info的适配器
    List<Person> Persons;
    LinearLayoutManager linearLayoutManager;
    RecyclerView recyclerView;

    EditText editText;//输入界面
    String tel = "";

    //播放音乐
    private MediaPlayer mediaPlayer1;
    private MediaPlayer mediaPlayer2;
    private MediaPlayer mediaPlayer3;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.call_fragment, container, false);
        LitePal.getDatabase();
        Persons = DataSupport.findAll(Person.class);
        load();


        //运行时权限处理，获取播放音频权限,电话/文件读取与写入
        if (ContextCompat.checkSelfPermission(view.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED|| ContextCompat.checkSelfPermission(view.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_CONTACTS}, 3);
        }


        editText = view.findViewById(R.id.search_1);
        editText.setText("");
        Button button0 = view.findViewById(R.id.Dial_0);
        Button button1 = view.findViewById(R.id.Dial_1);
        Button button2 = view.findViewById(R.id.Dial_2);
        Button button3 = view.findViewById(R.id.Dial_3);
        Button button4 = view.findViewById(R.id.Dial_4);
        Button button5 = view.findViewById(R.id.Dial_5);
        Button button6 = view.findViewById(R.id.Dial_6);
        Button button7 = view.findViewById(R.id.Dial_7);
        Button button8 = view.findViewById(R.id.Dial_8);
        Button button9 = view.findViewById(R.id.Dial_9);
        Button buttona = view.findViewById(R.id.Dial_a);
        Button buttonb = view.findViewById(R.id.Dial_b);
        Button buttoncall = view.findViewById(R.id.call);
        Button buttondel = view.findViewById(R.id.del);
        button0.setOnClickListener(this);
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
        button6.setOnClickListener(this);
        button7.setOnClickListener(this);
        button8.setOnClickListener(this);
        button9.setOnClickListener(this);
        buttona.setOnClickListener(this);
        buttonb.setOnClickListener(this);



        buttoncall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call();
            }
        });

        buttondel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editText.getText().toString().length()!=0){
                    tel="";//唯一用到的地方致空
                    tel=editText.getText().toString().substring(0,editText.getText().toString().length()-1);
                    editText.setText(tel);
                    Persons = DataSupport.select("name","Tel","Email").where("Tel like ?", "%" + editText.getText().toString() + "%").find(Person.class);
                    load();
                }
            }
        });
        return view;
    }





    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.Dial_0:

                //对0按键对应的mediaPlayer对象创建并初始化赋予音乐,得到初始化后的mediaPlayer，进行播放
                initMediaPlayer(mediaPlayer1,R.raw.button1).start();

                editText.setText(editText.getText()+"0");
                Persons = DataSupport.select("name","Tel","Email").where("Tel like ?", "%" + editText.getText().toString() + "%").find(Person.class);
                load();
                break;
            case R.id.Dial_1:
                initMediaPlayer(mediaPlayer2,R.raw.button2).start();
                editText.setText(editText.getText()+"1");
                Persons = DataSupport.select("name","Tel","Email").where("Tel like ?", "%" + editText.getText().toString() + "%").find(Person.class);
                load();
                break;
            case R.id.Dial_2:
                initMediaPlayer(mediaPlayer3,R.raw.button3).start();

                editText.setText(editText.getText()+"2");
                Persons = DataSupport.select("name","Tel","Email").where("Tel like ?", "%" + editText.getText().toString() + "%").find(Person.class);
                load();
                break;
            case R.id.Dial_3:
                initMediaPlayer(mediaPlayer1,R.raw.button1).start();
                editText.setText(editText.getText()+"3");
                Persons = DataSupport.select("name","Tel","Email").where("Tel like ?", "%" + editText.getText().toString() + "%").find(Person.class);
                load();
                break;
            case R.id.Dial_4:
                initMediaPlayer(mediaPlayer2,R.raw.button2).start();
                editText.setText(editText.getText()+"4");
                Persons = DataSupport.select("name","Tel","Email").where("Tel like ?", "%" + editText.getText().toString() + "%").find(Person.class);
                load();
                break;
            case R.id.Dial_5:
                initMediaPlayer(mediaPlayer3,R.raw.button3).start();
                editText.setText(editText.getText()+"5");
                Persons = DataSupport.select("name","Tel","Email").where("Tel like ?", "%" + editText.getText().toString() + "%").find(Person.class);
                load();
                break;
            case R.id.Dial_6:
                initMediaPlayer(mediaPlayer1,R.raw.button1).start();
                editText.setText(editText.getText()+"6");
                Persons = DataSupport.select("name","Tel","Email").where("Tel like ?", "%" + editText.getText().toString() + "%").find(Person.class);
                load();
                break;
            case R.id.Dial_7:
                initMediaPlayer(mediaPlayer2,R.raw.button2).start();
                editText.setText(editText.getText()+"7");
                Persons = DataSupport.select("name","Tel","Email").where("Tel like ?", "%" + editText.getText().toString() + "%").find(Person.class);
                load();
                break;
            case R.id.Dial_8:
                initMediaPlayer(mediaPlayer3,R.raw.button3).start();
                editText.setText(editText.getText()+"8");
                Persons = DataSupport.select("name","Tel","Email").where("Tel like ?", "%" + editText.getText().toString() + "%").find(Person.class);
                load();
                break;
            case R.id.Dial_9:
                initMediaPlayer(mediaPlayer1,R.raw.button1).start();
                editText.setText(editText.getText()+"9");
                Persons = DataSupport.select("name","Tel","Email").where("Tel like ?", "%" + editText.getText().toString() + "%").find(Person.class);
                load();
                break;
            case R.id.Dial_a:
                initMediaPlayer(mediaPlayer2,R.raw.button2).start();
                editText.setText(editText.getText()+"#");
                Persons = DataSupport.select("name","Tel","Email").where("Tel like ?", "%" + editText.getText().toString() + "%").find(Person.class);
                load();
                break;
            case R.id.Dial_b:
                initMediaPlayer(mediaPlayer3,R.raw.button3).start();
                editText.setText(editText.getText()+"*");
                Persons = DataSupport.select("name","Tel","Email").where("Tel like ?", "%" + editText.getText().toString() + "%").find(Person.class);
                load();
                break;
            default:
                break;
        }
    }


    //每个按钮点击时完成初始化，第一个参数是MediaPlayer对象，第二个是音乐s的id
    private MediaPlayer initMediaPlayer(MediaPlayer mediaPlayer,int R){
        try {
            mediaPlayer = MediaPlayer.create(view.getContext().getApplicationContext(),R);
            if (mediaPlayer!= null) {
                mediaPlayer.stop();
            }
            mediaPlayer.prepare();//准备好
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mediaPlayer;
    }


// 打电话的逻辑
    private void call() {
        try {
//            Toast.makeText(view.getContext(),editText.getText().toString(),Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Intent.ACTION_CALL);//调用系统内置好的action来启动系统活动（隐式Intent的更多用法）
            intent.setData(Uri.parse("tel:"+editText.getText().toString()));//设置一个Uri对象作为操作数据。这个数据可以被定义额外定义data的活动响应
            startActivity(intent);

            recording recording=new recording();
            recording.setName("未知的名字");
            recording.setTel(editText.getText().toString());
            recording.setPosition(getGeo(editText.getText().toString()));
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
            recording.setTime(df.format(new Date()));//new Date()为获取当前系统时间
            recording.setStatus(getCarrier(view.getContext(), editText.getText().toString(),86));
            recording.save();


        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();//刷新一下
    }

    //  无论授权是否通过都会加载的逻辑
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 3:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    //否则不让使用
                    Toast.makeText(view.getContext(), "拒绝权限将无法使用程序", Toast.LENGTH_SHORT).show();
                    MainActivity activity=(MainActivity)getActivity();//碎片中获取当前活动
                    activity.finish();
                }
                break;
            default:
        }
    }

    public void load(){
        recyclerView = view.findViewById(R.id.recycler_view);
        linearLayoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new PersonAdapter_info(Persons);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mediaPlayer1 != null) {
            //将相关资源释放掉
            mediaPlayer1.stop();
            mediaPlayer1.release();
        }
    }

}
