package com.pan.coursedesign;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pan.coursedesign.recyclerview.Person;
import com.pan.coursedesign.recyclerview_logs.recording;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.json.JSONArray;
import org.json.JSONException;
//这俩巨坑！！！！！！
//import com.alibaba.fastjson.JSONObject;
import org.json.JSONObject;
import org.litepal.crud.DataSupport;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Map;
import java.util.Set;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    AlertDialog.Builder builder5;//自定义
    AlertDialog dialog;
    private int tag = 0;
    private int DataDownTag = 0;
    private DrawerLayout mDrawerLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //自定义Toolbar并设置进系统
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //因为滑动菜单不容易被用户知道，所以加一个折叠按钮
        ActionBar actionBar = getSupportActionBar();//得到actionBar的实例（toolbar实现的哦，也就是toolbar对象）
        if (actionBar != null) { //有的话
            actionBar.setDisplayHomeAsUpEnabled(true); //显示一个home按钮，HomeAsUp这个按钮的id永远是Home
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu); //设置一个图标
        }

        //获取抽屉的实例用于打开和关闭抽屉的事件
        mDrawerLayout = findViewById(R.id.my_drawer);

        //默认加载的碎片，text1
        replaceFragment(new Contacts_Fragment());


        //启动弹窗
        //从文件读取tag数据
        SharedPreferences preferences = getSharedPreferences("statata", MODE_PRIVATE);
        tag = preferences.getInt("tag", 0);
        if (tag == 0) { //tag没被修改，显示弹窗
            loding();
        }


        //设置侧面munu的点击事件
        NavigationView navigationView = (NavigationView) findViewById(R.id.my_nav_view);
        navigationView.setCheckedItem(R.id.nav_text1);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                //详细设计抽屉菜单的点击事件
                switch (item.getItemId()) {
                    case R.id.nav_text1:
                        replaceFragment(new Contacts_Fragment());
                        mDrawerLayout.closeDrawer(navigationView);
                        break;
                    case R.id.nav_text2:
                        replaceFragment(new Call_Fragment());
                        mDrawerLayout.closeDrawer(navigationView);
                        break;
                    case R.id.nav_text3:
                        replaceFragment(new Call_logs_Fragment());
                        mDrawerLayout.closeDrawer(navigationView);
                        break;
                    case R.id.nav_text4:
                        replaceFragment(new Help_Fragment());
                        mDrawerLayout.closeDrawer(navigationView);
                        break;
                    case R.id.nav_text5:
                        replaceFragment(new About_Fragment());
                        mDrawerLayout.closeDrawer(navigationView);
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
    }


    //动态替换碎片
    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.DTFragment, fragment);
        transaction.commit();
    }

// ----toolbar逻辑----

    //传统的方法实现菜单的植入（command + O快捷键）
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //设置一个menu
        getMenuInflater().inflate(R.menu.toolbar, menu);
        return true;
    }


    //tab按键事件
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //设置menu的选项点击事件
        switch (item.getItemId()) {
            case android.R.id.home://这是内置的哦
                mDrawerLayout.openDrawer(GravityCompat.START);//将滑动菜单显示出来，打开抽屉 参数是方式和xml设置的保持一致
                break;
            case R.id.DataDown:
                DataDown();//阿帕奇服务器上下载json数据
                break;
            case R.id.DataUp:
                try {
                    UpData(GetJsonString());//先把数据导出成json文本在作为数据上传到服务器
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//                Toast.makeText(this,"此功能开发中...",Toast.LENGTH_LONG).show();
                break;
            case R.id.settings:
                try {
                    ExportData();
                    Toast.makeText(this, "导出成功: 内部目录file文件夹下", Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.settings_1:
                if (Import().isEmpty()) {
                    Toast.makeText(this, "导入失败，没有数据", Toast.LENGTH_LONG).show();
                } else {
                    ImportData();
                    Toast.makeText(this, "导入成功", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.settings_2:
                delall();
                break;
            default:
        }
        return true;
    }


    //1.下载时提示框
    public void DataDown() {
        //启动弹窗
        //从文件读取tag数据
        SharedPreferences preferences = getSharedPreferences("DataDown", MODE_PRIVATE);
        DataDownTag = preferences.getInt("DataDownTag", 0);
        if (DataDownTag == 0) { //tag没被修改，显示弹窗
            //inflate方法返回的是一个view（自定义那个），
            View loginview = LayoutInflater.from(this).inflate(R.layout.datadown_dialog_info, null);
            //loginview里的控件声明一下
            CheckBox checkBox = loginview.findViewById(R.id.info_2);
            Button btnLogin = loginview.findViewById(R.id.datadown);

            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //把状态计入文件供下次读取
                    if (checkBox.isChecked()) {//如果被选中
                        SharedPreferences.Editor editor = getSharedPreferences("DataDown", MODE_MULTI_PROCESS).edit();
                        editor.putInt("DataDownTag", 1);
                        editor.apply();
                    }
                    dialog.dismiss();
                    DownData();
                }
            });
            //设置Dialog和View
            builder5 = new AlertDialog.Builder(this);
            dialog = builder5.setTitle("重要提示").setView(loginview).setCancelable(false).show();
        } else {
            DownData();
        }
    }

    //从服务器下载json数据
    public void DownData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder()
                            // 指定访问的服务器地址是电脑本机
                            .url("http://172.20.10.5:8081/pro")
                            .build();
                    Response response = client.newCall(request).execute();
                    String responseData = response.body().string();
                    parseJSONWithGSON(responseData);//下载json数据导入解析
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    //导入json进行解析的方法
    private void parseJSONWithGSON(String jsonData) {

        //Log.d("aaaaa---", jsonData);
        Contacts_Fragment contacts_fragment = (Contacts_Fragment) getSupportFragmentManager().findFragmentById(R.id.DTFragment);
        Gson gson = new Gson();
        List<Person> appList = gson.fromJson(jsonData, new TypeToken<List<Person>>() {
        }.getType());
        //c=appList;
        int tag = 0;
        //遍历并先把数据存入数据库
        for (Person person : appList) {
            person.setTel(formatInviteCode(person.getTel()));//获取的数据同样要处理
            if (RemoveRepetition(person.getName())) { //只有数据库没有时才更新
                contacts_fragment.Persons.add(person);
                person.save();
                tag = 1;
            }
        }
        if (tag == 1) {//只要有一个变就刷新
            //这个方法是在任务线程执行的，需要去主线程更新ui
            //加个下载成功的通知
            DataNotification();
            MainActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    contacts_fragment.load();
                }
            });
        } else {
            DataFailNotification();//找不到服务器/导入的数据都有
        }
    }

    public Boolean RemoveRepetition(String name) {
        List<Person> Persons = DataSupport.select("name", "Tel", "Email").where("name like ?", "%" + name + "%").find(Person.class);
        if (Persons.size() != 0) {
            return false;//如果数据库有，不更新
        }
        return true;
    }

    //下载成功的通知
    public void DataNotification() {
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification notification = new NotificationCompat.Builder(this, "5996773")
                .setContentTitle("文件下载的通知")//标题内容
                .setContentText("导入数据成功!")//正文内容
                .setWhen(System.currentTimeMillis())//指定通知被创建的时间
                .setSmallIcon(R.mipmap.ic_launcher)//设置通知小图标
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))//设置通知大图标
                .setVibrate(new long[]{0, 1000, 1000, 1000})//通知到了让手机震动
                .setLights(Color.GREEN, 1000, 1000)//设置手机前置LED灯的显示效果
                .setPriority(NotificationCompat.PRIORITY_MAX)//设置通知的重要程度
                .build();//build完算是构建完成notification对象
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("5996773", "安卓10的通知哦", NotificationManager.IMPORTANCE_HIGH);
            channel.enableLights(true);//是否在桌面icon右上角展示小红点
            channel.setLightColor(Color.GREEN);//小红点颜色
            channel.setShowBadge(false); //是否在久按桌面图标时显示此渠道的通知
            manager.createNotificationChannel(channel);//将channel设置进manager
        }
        //id唯一即可
        manager.notify(1, notification);
    }

    //下载失败的通知
    public void DataFailNotification() {
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification notification = new NotificationCompat.Builder(this, "5996774")
                .setContentTitle("文件下载的通知")//标题内容
                .setContentText("文件下载成功，但您不需要导入!")//正文内容
                .setWhen(System.currentTimeMillis())//指定通知被创建的时间
                .setSmallIcon(R.mipmap.ic_launcher)//设置通知小图标
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))//设置通知大图标
                .setVibrate(new long[]{0, 1000, 1000, 1000})//通知到了让手机震动
                .setLights(Color.GREEN, 1000, 1000)//设置手机前置LED灯的显示效果
                .setPriority(NotificationCompat.PRIORITY_MAX)//设置通知的重要程度
                .build();//build完算是构建完成notification对象
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("5996774", "安卓10的通知哦", NotificationManager.IMPORTANCE_HIGH);
            channel.enableLights(true);//是否在桌面icon右上角展示小红点
            channel.setLightColor(Color.GREEN);//小红点颜色
            channel.setShowBadge(false); //是否在久按桌面图标时显示此渠道的通知
            manager.createNotificationChannel(channel);//将channel设置进manager
        }
        //id唯一即可
        manager.notify(2, notification);
    }


    //2.上传json到服务器
    public void  UpData(String body){
        new Thread(new Runnable(){
            @Override
            public void run(){
                String url="http://172.20.10.5:8081/air";//你要訪問的地址
                String mesg = null;//定义服务器返回的结果
                HttpClient httpClient = new HttpClient();//构建HttpClient实例
                httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(60000); //设置请求超时时间
                httpClient.getHttpConnectionManager().getParams().setSoTimeout(60000);//设置响应超时时间
                PostMethod postMethod=new PostMethod(url);//构造PostMethod的实例
                postMethod.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET,"utf-8");
                String jsonString="{'data':" + body + "}";
                //com.alibaba.fastjson.JSONObject意思是导入阿里的parseObject,parseObject第一个参数是json对象
                Map<String,Object> map = com.alibaba.fastjson.JSONObject.parseObject(jsonString,Map.class);
                Set<String> set = map.keySet();
                for(String s : set){
                    postMethod.addParameter(s,map.get(s).toString());
                }
                try {
                    httpClient.executeMethod(postMethod);//执行post请求
                    mesg = postMethod.getResponseBodyAsString();//可以对响应回来的报文进行处理
                    //Log.d("aaaa--", mesg);//获取成功！！！！
                } catch (HttpException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }finally{
                    if(mesg!=null){//有数据
                        DataupNotification();//文件上传成功的通知
                    }
                    //关闭连接释放资源的方法
                    postMethod.releaseConnection();
                    httpClient.getHttpConnectionManager().closeIdleConnections(0);
                }
            }
        }).start();
    }

    //上传成功的通知
    public void DataupNotification() {
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification notification = new NotificationCompat.Builder(this, "599111")
                .setContentTitle("文件上传的通知")//标题内容
                .setContentText("上传数据成功!")//正文内容
                .setWhen(System.currentTimeMillis())//指定通知被创建的时间
                .setSmallIcon(R.mipmap.ic_launcher)//设置通知小图标
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))//设置通知大图标
                .setVibrate(new long[]{0, 1000, 1000, 1000})//通知到了让手机震动
                .setLights(Color.GREEN, 1000, 1000)//设置手机前置LED灯的显示效果
                .setPriority(NotificationCompat.PRIORITY_MAX)//设置通知的重要程度
                .build();//build完算是构建完成notification对象
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("599111", "安卓10的通知哦", NotificationManager.IMPORTANCE_HIGH);
            channel.enableLights(true);//是否在桌面icon右上角展示小红点
            channel.setLightColor(Color.GREEN);//小红点颜色
            channel.setShowBadge(false); //是否在久按桌面图标时显示此渠道的通知
            manager.createNotificationChannel(channel);//将channel设置进manager
        }
        //id唯一即可
        manager.notify(3, notification);
    }


    //3.导出json到文件
    public void ExportData() throws JSONException {
        String str = GetJsonString();
        save(str);
    }

    //导出json文本
    public String GetJsonString() throws JSONException {
        //获取碎片实例以调用碎片中的方法
        Contacts_Fragment contacts_fragment = (Contacts_Fragment) getSupportFragmentManager().findFragmentById(R.id.DTFragment);
        List<Person> Persons = contacts_fragment.Persons;

        //转换list为json
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();//这个巨坑，注意包
        JSONObject tmpObj = null;
        int count = Persons.size();
        for (int i = 0; i < count; i++) {
            tmpObj = new JSONObject();
            tmpObj.put("name", Persons.get(i).getName());
            tmpObj.put("Email", Persons.get(i).getEmail());
            tmpObj.put("Tel", Persons.get(i).getTel());
            jsonArray.put(tmpObj);
            tmpObj = null;
        }
        String personInfos = jsonArray.toString(); // 将JSONArray转换得到String
        //Log.d("DDDDD",personInfos);
        return personInfos;
    }


    //将一段json文本保存到文件中去
    public void save(String inputText) {
        FileOutputStream out = null;
        BufferedWriter writer = null;
        //command + alt + t 快速生成
        try {
            File file = new File(getCacheDir().getPath(), "data.json");
            if (file != null) {
                file.delete();
            } else {
                file.createNewFile();
            }
            out = openFileOutput("data.json", Context.MODE_PRIVATE);
//             out = new FileOutputStream(file);
            // OutputStreamWriter是从字符流到字节流的桥接：字节流搞成了字符流传递给缓冲流
            writer = new BufferedWriter(new OutputStreamWriter(out));
            // 写入
            writer.write(inputText);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (writer != null) {
                    writer.close();//关闭上层流（缓冲流），下层数据流就自动关了
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    //4.从文件导入json
    public void ImportData() {
        String str1 = Import();
        parseJSONWithGSON(str1);//显示
    }


    //从文件中读取json文件
    public String Import() {
        //准备从文件读数据
        FileInputStream in = null;
        //缓冲流，输入
        BufferedReader reader = null;
        StringBuilder content = new StringBuilder();

        try {
            in = openFileInput("data.json");
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            //reader.readLine()是读取一行
            while ((line = reader.readLine()) != null) {
                content.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return content.toString();
    }

    //初始化加载
    private void loding() {
        //inflate方法返回的是一个view（自定义那个），
        View loginview = LayoutInflater.from(this).inflate(R.layout.ui_dialog_info, null);
        //loginview里的控件声明一下
        CheckBox checkBox = loginview.findViewById(R.id.info_2);
        Button btnLogin = loginview.findViewById(R.id.info_3);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//              把状态计入文件供下次读取
                if (checkBox.isChecked()) {//如果被选中
                    SharedPreferences.Editor editor = getSharedPreferences("statata", MODE_MULTI_PROCESS).edit();
                    editor.putInt("tag", 1);
                    editor.apply();
                }
                dialog.dismiss();
            }
        });
        //设置Dialog和View
        builder5 = new AlertDialog.Builder(this);
        dialog = builder5.setTitle("重要信息").setView(loginview).setCancelable(false).show();
    }


    //5.一键删除
    public void delall() {
        DataSupport.deleteAll(Person.class);
        DataSupport.deleteAll(recording.class);
        Toast.makeText(this, "删除全部数据成功！", Toast.LENGTH_SHORT).show();
    }



    //处理一下系统获取联系人时字符串的-/空格
    public String formatInviteCode(String str1) {
        String str2 = "";
        for (int i = 0; i < str1.length(); i++) {
            if (Character.isDigit(str1.charAt(i))) {//判断被索引处的字符是不是数字
                str2 += str1.charAt(i);
            }
        }
        return str2;
    }
}