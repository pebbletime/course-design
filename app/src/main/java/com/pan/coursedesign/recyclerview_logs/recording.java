package com.pan.coursedesign.recyclerview_logs;

import org.litepal.crud.DataSupport;

public class recording extends DataSupport {
    private String name;
    private String tel;
    private String time;
    private String status;
    private String position;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }



    @Override
    public String toString() {
        return "recording{" +
                "name='" + name + '\'' +
                ", tel='" + tel + '\'' +
                ", time='" + time + '\'' +
                ", status='" + status + '\'' +
                ", position='" + position + '\''+
                '}';
    }


}
