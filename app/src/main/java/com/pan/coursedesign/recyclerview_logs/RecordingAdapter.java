package com.pan.coursedesign.recyclerview_logs;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.pan.coursedesign.R;

import org.litepal.crud.DataSupport;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.pan.coursedesign.utils.MobileNumberUtils.getCarrier;
import static com.pan.coursedesign.utils.MobileNumberUtils.getGeo;

public class RecordingAdapter extends RecyclerView.Adapter<RecordingAdapter.ViewHolder> {

    private List<recording> mRecording_List;


    public RecordingAdapter(List<recording> recording_List) {
        mRecording_List = recording_List;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        View recordingView;
        TextView recordingName;
        TextView recordingTime;
        TextView recordingStatus;
        TextView recordingPosition;
        Button button000;
        public ViewHolder(View view) {
            super(view);
            recordingView = view;
            recordingName = (TextView) view.findViewById(R.id.recording_name);
            recordingTime = (TextView) view.findViewById(R.id.recording_time);
            recordingStatus = (TextView) view.findViewById(R.id.recording_status);
            recordingPosition = (TextView) view.findViewById(R.id.recording_position);
            button000 = view.findViewById(R.id.btn_del);
        }
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.call_recording_item, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        holder.recordingView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                recording recording = mRecording_List.get(position);
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:"+recording.getTel()));
                v.getContext().startActivity(intent);

                recording recording1=new recording();
                recording1.setName(recording.getName());
                recording1.setTel(recording.getTel());
                recording1.setPosition("河南");
                recording1.setPosition(getGeo(recording.getTel()));
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
                recording1.setTime(df.format(new Date()));//new Date()为获取当前系统时间
                recording1.setStatus(getCarrier(parent.getContext(), recording.getTel(),86));
                recording1.save();
            }
        });

        holder.button000.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                recording recording = mRecording_List.get(position);
                mRecording_List.remove(recording);
                String name = recording.getName();
                DataSupport.deleteAll(recording.class,"name = ?", name);
                Toast.makeText(v.getContext(), "删除 " + recording.getName() + "成功！", Toast.LENGTH_SHORT).show();
                notifyItemRemoved(position);
            }
        });
        return holder;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        recording recording = mRecording_List.get(position);
        holder.recordingName.setText(recording.getName());
        holder.recordingTime.setText(recording.getTime());
        holder.recordingStatus.setText(recording.getStatus());
        holder.recordingPosition.setText(recording.getPosition());
    }


    @Override
    public int getItemCount() {
        return mRecording_List.size();
    }

}