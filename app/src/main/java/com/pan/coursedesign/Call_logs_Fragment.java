package com.pan.coursedesign;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.pan.coursedesign.recyclerview_logs.RecordingAdapter;
import com.pan.coursedesign.recyclerview_logs.recording;

import org.litepal.LitePal;
import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

public class Call_logs_Fragment extends Fragment {
    RecordingAdapter adapter;//指定info的适配器
    List<recording> recordings = new ArrayList<recording>();//数据
    LinearLayoutManager linearLayoutManager;
    RecyclerView recyclerView;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.call_logs_fragment, container, false);

        LitePal.getDatabase();//创建数据库
        recordings = DataSupport.findAll(recording.class);
        load();//加载
        return view;
    }

    public void load(){
        recyclerView = view.findViewById(R.id.recycler_view_logs);
        linearLayoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new RecordingAdapter(recordings);
        recyclerView.setAdapter(adapter);
    }

}
