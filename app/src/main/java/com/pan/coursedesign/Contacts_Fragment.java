package com.pan.coursedesign;

import android.Manifest;
import android.app.AlertDialog;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pan.coursedesign.recyclerview.Person;
import com.pan.coursedesign.recyclerview.PersonAdapter;


import org.litepal.LitePal;
import org.litepal.crud.DataSupport;

import java.util.List;


public class Contacts_Fragment extends Fragment {
    PersonAdapter adapter;
    List<Person> Persons;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    AlertDialog.Builder builder5;//自定义
    AlertDialog dialog;
    View view;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.contacts_fragment, container, false);

        LitePal.getDatabase();//创建数据库
        //联系人授权和 直接拨打电话授权
        if (ContextCompat.checkSelfPermission(view.getContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(view.getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{ Manifest.permission.READ_CONTACTS,Manifest.permission.CALL_PHONE}, 1);
        } else {
            readContacts();
        }


        //搜索按钮事件
        //获取到布局文件定义的元素
        Button button01 = view.findViewById(R.id.but_1);
        EditText editText = view.findViewById(R.id.search_1);
        //注册一个监听器，向setOnClickListener传递的是匿名内部类产生的对象
        button01.setOnClickListener(new View.OnClickListener() {  //OnClickListener()是View类里面的接口匿名内部类
            @Override
            public void onClick(View v) { //对接口匿名内部类进行实现
                String pro = editText.getText().toString();
                if(pro.isEmpty()){
                    Persons = DataSupport.select("name","Tel","Email").where().find(Person.class);
                    load();
                }else {
//                  模糊查询姓名
                    Persons = DataSupport.select("name","Tel","Email").where("name like ?", "%" + pro + "%").find(Person.class);
                    load();
                }
            }
        });

        //添加按钮按钮
        Button button02 = view.findViewById(R.id.but_2);
        button02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //inflate方法返回的是一个view（自定义那个），
                View loginview = LayoutInflater.from(view.getContext()).inflate(R.layout.contacts_dialog_info, null);
                //loginview里的控件声明一下
                EditText username = loginview.findViewById(R.id.et_username);
                EditText password = loginview.findViewById(R.id.et_password);
                EditText email = loginview.findViewById(R.id.et_email);
                Button btnLogin = loginview.findViewById(R.id.btn_login);
                Button btnLogin2 = loginview.findViewById(R.id.btn_text);
                btnLogin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //存储信息
                        Person Person = new Person();
                        Person.setName(username.getText().toString());
                        Person.setTel(password.getText().toString());
                        Person.setEmail(email.getText().toString());
                        Person.save();
                        Toast.makeText(view.getContext(),"添加联系人成功",Toast.LENGTH_LONG).show();
//                      同时存储到数组中,必须存到数组adapter.notifyDataSetChanged();刷新才有效！！！
                        Persons.add(Person);
                        dialog.dismiss();//关闭自定义对话框
                    }
                });

//              一键添加数据
                btnLogin2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        text();//批量数据添加方法
                        Toast.makeText(view.getContext(),"添加成功",Toast.LENGTH_LONG).show();
                        dialog.dismiss();//关闭自定义对话框
                    }
                });

                //设置Dialog和View
                builder5 = new AlertDialog.Builder(view.getContext());
                dialog = builder5.setTitle("输入联系人信息").setView(loginview).show(); //这个show返回一个AlertDialog对象
            }

        });



        //创建完数据库进行展示，也是第一次加载时的数据展示
//      1.从数据库查找数据                                       准备数据
        Persons = DataSupport.findAll(Person.class);
        load();

        return view;
    }


    //1.读取联系人
    private void readContacts() {
        Cursor cursor = null;
        try {
            // 查询联系人数据，获取内容提供者对象，提供了接口用来查询,getActivity()获取主活动
            cursor = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    //获取联系人姓名
                    String displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    //获取联系人手机号
                    String number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    Person persons1 = new Person();
                    persons1.setName(displayName);
                    persons1.setTel(formatInviteCode(number));//只保留号码中的数字
                    if (Persons!=null){
                        Persons.add(persons1);
                        persons1.save();//保存到数据库
                    }
                }
//              list数据改变刷新页面
                if (adapter!=null){
                    adapter.notifyDataSetChanged();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    //无论授权是否通过都会加载的逻辑
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                  授权了则加载联系人
                    readContacts();
                } else {
//                  没授权直接不给用
                    Toast.makeText(view.getContext(), "你没有授权哦兄der！", Toast.LENGTH_SHORT).show();
                    MainActivity activity=(MainActivity)getActivity();//碎片中获取当前活动
                    activity.finish();
                }
                break;
            default:
        }
    }





    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();//刷新一下
    }



    private void text(){
        //存储信息
        Person Person = new Person();
        Person.setName("潘乐乐");
        Person.setEmail("2270057439@qq.com");
        Person.setTel("17772113735");
        //同时存储到数组中,必须存到数组adapter.notifyDataSetChanged();刷新才有效！！！
        Persons.add(Person);
        Person.save();

        Person Person2 = new Person();
        Person2.setName("陈一");
        Person2.setEmail("1111@qq.com");
        Person2.setTel("1111");
        Persons.add(Person2);
        Person2.save();

        Person Person3 = new Person();
        Person3.setName("徐二");
        Person3.setEmail("2222@qq.com");
        Person3.setTel("2222");
        Persons.add(Person3);
        Person3.save();

        Person Person4 = new Person();
        Person4.setName("张三");
        Person4.setEmail("333@qq.com");
        Person4.setTel("333");
        Persons.add(Person4);
        Person4.save();

        Person Person5 = new Person();
        Person5.setName("李四");
        Person5.setEmail("4444@qq.com");
        Person5.setTel("444444");
        Persons.add(Person5);
        Person5.save();

        Person Person6 = new Person();
        Person6.setName("王五");
        Person6.setEmail("5555@qq.com");
        Person6.setTel("5555");
        Persons.add(Person6);
        Person6.save();

        Person Person7 = new Person();
        Person7.setName("马六");
        Person7.setEmail("6666@qq.com");
        Person7.setTel("66666");
        Persons.add(Person7);
        Person7.save();
        adapter.notifyDataSetChanged();
    }


    public void load(){
        //2.通过反射获取item布局实例                                准备布局
        recyclerView = view.findViewById(R.id.recycler_view);
        //3.指定布局方式
        linearLayoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        //4.传入水果数据到PersonAdapter构造函数创建adapter实例        通过适配器将数据与布局进行映射
        adapter = new PersonAdapter(Persons);
        //5.完成适配器设置，关联映射完成，输出画面                     完成映射
        recyclerView.setAdapter(adapter);
    }


    //处理一下系统获取联系人时字符串的-/空格
    public String formatInviteCode(String str1) {
        String str2 = "";
        for (int i = 0; i < str1.length(); i++) {
            if (Character.isDigit(str1.charAt(i))) {//判断被索引处的字符是不是数字
                str2 += str1.charAt(i);
            }
        }
        return str2;
    }

}
