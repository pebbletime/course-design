package com.pan.coursedesign;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pan.coursedesign.recyclerview_logs.RecordingAdapter;
import com.pan.coursedesign.recyclerview_logs.recording;

import java.util.ArrayList;
import java.util.List;


public class About_Fragment extends Fragment {
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.about_webview, container, false);

        WebView webView = (WebView) view.findViewById(R.id.web_view);

        //让内置浏览器支持js脚本
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl("https://pan-le.cn");
        return view;
    }

}
